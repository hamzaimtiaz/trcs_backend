from __future__ import unicode_literals
from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Company)
admin.site.register(Commodity)
admin.site.register(Bank)
admin.site.register(Import)
admin.site.register(Payments)