from trcs.models import *
from trcs.serializers import *
from rest_framework import generics
import django_filters
# Create your views here.



class CompanyList(generics.ListCreateAPIView):
    queryset = Company.objects.all().order_by('-creation_time')
    serializer_class = CompanySerializer


class CompanyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all().order_by('-creation_time')
    serializer_class = CompanySerializer

class CommodityCreate(generics.CreateAPIView):
    queryset = Commodity.objects.all().order_by('-creation_time')
    serializer_class = CommodityCreateUpdateSerializer

class CommodityUpdate(generics.UpdateAPIView):
    queryset = Commodity.objects.all().order_by('-creation_time')
    serializer_class = CommodityCreateUpdateSerializer

class CommodityList(generics.ListAPIView):
    queryset = Commodity.objects.all().order_by('-creation_time')
    serializer_class = CommodityReadSerializer
    filterset_fields = ['company']

class CommodityDetail(generics.RetrieveDestroyAPIView):
    queryset = Commodity.objects.all().order_by('-creation_time')
    serializer_class = CommodityReadSerializer

# class CommodityList(generics.ListCreateAPIView):
#     queryset = Commodity.objects.all().order_by('-creation_time')
#     serializer_class = CommodityReadSerializer
#     filterset_fields = ['company']




# class CommodityDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Commodity.objects.all().order_by('-creation_time')
#     serializer_class = CommoditySerializer


class BankList(generics.ListCreateAPIView):
    queryset = Bank.objects.all().order_by('-creation_time')
    serializer_class = BankSerializer


class BankDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bank.objects.all().order_by('-creation_time')
    serializer_class = BankSerializer

class ImportCreate(generics.CreateAPIView):
    queryset = Import.objects.all().order_by('-creation_time')
    serializer_class = ImportCreateUpdateSerializer

    
class ImportUpdate(generics.UpdateAPIView):
    queryset = Import.objects.all().order_by('-creation_time')
    serializer_class = ImportCreateUpdateSerializer

class ImportFilter(django_filters.FilterSet):
    start_date = django_filters.DateFilter(field_name="date", lookup_expr="gte")
    end_date = django_filters.DateFilter(field_name="date", lookup_expr="lte")
    

    class Meta:
        model = Import
        fields = [
            "company",
            "commodity",
            "bank",
            "transaction_type",
            "BL_number",
            "start_date",
            "end_date",
        ]
class ImportList(generics.ListAPIView):
    queryset = Import.objects.all().order_by('-creation_time')
    serializer_class = ImportReadSerializer
    filterset_class = ImportFilter
    # filterset_fields = ['company','commodity','bank','transaction_type','date','BL_number']


class ImportDetail(generics.RetrieveDestroyAPIView):
    queryset = Import.objects.all().order_by('-creation_time')
    serializer_class = ImportReadSerializer

class PaymentsCreate(generics.CreateAPIView):
    queryset = Payments.objects.all().order_by('-creation_time')
    serializer_class = PaymentsCreateUpdateSerializer

class PaymentsUpdate(generics.UpdateAPIView):
    queryset = Payments.objects.all().order_by('-creation_time')
    serializer_class = PaymentsCreateUpdateSerializer

class PaymentsFilter(django_filters.FilterSet):
    start_date = django_filters.DateFilter(field_name="date", lookup_expr="gte")
    end_date = django_filters.DateFilter(field_name="date", lookup_expr="lte")
    

    class Meta:
        model = Payments
        fields = [
            "company",
            "bank",
            "payment_type",
            "start_date",
            "end_date",
        ]
class PaymentsList(generics.ListAPIView):
    queryset = Payments.objects.all().order_by('-creation_time')
    serializer_class = PaymentsReadSerializer
    filterset_class = PaymentsFilter
    # filterset_fields = ['company','bank','payment_type','start_date','end_date']

class PaymentsDetail(generics.RetrieveDestroyAPIView):
    queryset = Payments.objects.all().order_by('-creation_time')
    serializer_class = PaymentsReadSerializer