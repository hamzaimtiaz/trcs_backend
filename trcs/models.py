from django.db import models
# from __future__ import unicode_literals
from django.contrib.auth.models import User
# Create your models here.

class Company(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    contactNo = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    # balance = models.FloatField(default=0)

    class Meta:
        ordering = ['creation_time']

    def __str__(self):
        return self.name

class Commodity(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    name = models.CharField(max_length=100)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='commodity')

    class Meta:
        ordering = ['creation_time']

    def __str__(self):
        return self.name

class Bank(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)

    class Meta:
        ordering = ['creation_time']

    def __str__(self):
        return self.name

class Import(models.Model):
    TRANSACTION_CHOICES = (
        ('1', 'Sale'),
        ('2', 'Purchase'),
    )
    
    
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='imprt')
    commodity = models.ForeignKey(Commodity, on_delete=models.CASCADE, related_name='imprt')
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE, related_name='imprt')
    weight = models.FloatField()
    
    transaction_type = models.CharField(max_length=100,choices = TRANSACTION_CHOICES)
    date = models.DateField()
    price = models.FloatField()
    
    other_expense = models.FloatField(default=0)
    container_no1 = models.CharField(max_length=100,blank=True, null=True)
    container_no1_weight = models.FloatField(default=0)
    container_no2 = models.CharField(max_length=100,blank=True, null=True)
    container_no2_weight = models.FloatField(default=0)
    container_no3 = models.CharField(max_length=100,blank=True, null=True)
    container_no3_weight = models.FloatField(default=0)
    container_no4 = models.CharField(max_length=100,blank=True, null=True)
    container_no4_weight = models.FloatField(default=0)
    container_no5 = models.CharField(max_length=100,blank=True, null=True)
    container_no5_weight = models.FloatField(default=0)
    
    bank_charges = models.FloatField(default=0,blank=True, null=True)
    BL_number = models.CharField(max_length=100)
    cont = models.CharField(max_length=100,null=True,blank= True)
    transaction_number = models.CharField(max_length=100,null=True, blank=True)
    shipper = models.CharField(max_length=100,null=True)
    arrival_date = models.DateField(null= True,blank= True)
    clearing_charges = models.FloatField(default=0)
    dollar_rate = models.FloatField(default=0)

    weight = models.FloatField(default=0)
    contract_no = models.CharField(max_length=100,null=True)
    wh_tax = models.FloatField(default=0)
    lc_commission = models.FloatField(default=0)
    accountant_charges = models.FloatField(default=0)
    insurance = models.FloatField(default=0)
    broker_charges = models.FloatField(default=0)

class Payments(models.Model):
    
    PAYMENT_CHOICES = (
        ('1', 'Received'),
        ('2', 'Sent'),
    )
    PAYMENT_TYPE_CHOICES = (
        ('1', 'Debit'),
        ('2', 'Credit'),
        ('3', 'Cash'),
    )

    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='payment')
    container_no = models.CharField(max_length=100, blank= True , null= True)
    date = models.DateField()
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE, related_name='payment')
    amount = models.FloatField()
    # prev_balance = models.FloatField(null = True)
    # balance = models.FloatField(null = True)
    payment_type = models.CharField(max_length=100,choices = PAYMENT_CHOICES)
    payment_choice_type = models.CharField(max_length=100,choices = PAYMENT_TYPE_CHOICES)


