from rest_framework import serializers
from .models import *
from django.db.models import F, Q, Sum


class CompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = '__all__'


class CommodityCreateUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Commodity
        fields = '__all__'

class CommodityReadSerializer(serializers.ModelSerializer):

    company = CompanySerializer(read_only=True)
    
    class Meta:
        model = Commodity
        fields = '__all__'


class BankSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bank
        fields = '__all__'


class ImportCreateUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Import
        fields = '__all__'


class ImportReadSerializer(serializers.ModelSerializer):
    amount = serializers.SerializerMethodField()
    prev_balance = serializers.SerializerMethodField()
    balance = serializers.SerializerMethodField()
    container1_weight_difference = serializers.SerializerMethodField()
    container2_weight_difference = serializers.SerializerMethodField()
    container3_weight_difference = serializers.SerializerMethodField()
    container4_weight_difference = serializers.SerializerMethodField()
    container5_weight_difference = serializers.SerializerMethodField()
    # total_weight = serializers.SerializerMethodField()
    company = CompanySerializer(read_only=True)
    commodity = CommodityReadSerializer(read_only=True)
    bank = BankSerializer(read_only=True)
    transaction_type_name = serializers.SerializerMethodField()
    invoice = serializers.SerializerMethodField()


    class Meta:
        model = Import
        fields = ('id', 'company', 'commodity', 'bank',
                'container_no1','container_no1_weight','container_no2','container_no2_weight','container_no3','container_no3_weight',
                'container_no4','container_no4_weight','container_no5','container_no5_weight',
                'price', 'transaction_type','date', 'other_expense', 'transaction_number',
                'shipper', 'cont', 'invoice', 'arrival_date', 'BL_number', 'clearing_charges', 'dollar_rate',
                'amount', 'prev_balance', 'balance',
                'container1_weight_difference', 'container2_weight_difference' , 'container3_weight_difference',
                'container4_weight_difference','container5_weight_difference','bank_charges','weight',
                'contract_no','wh_tax','lc_commission','accountant_charges','insurance','broker_charges','transaction_type_name')

    def get_transaction_type_name(self, instance):
        return instance.get_transaction_type_display()

    # def get_total_weight(self, instance):
    #     return instance.container_no1_weight+instance.container_no2_weight+instance.container_no3_weight+instance.container_no4_weight+instance.container_no5_weight
    
    def get_amount(self, instance):
        if instance.transaction_type == '1':
            # return (instance.price*(instance.container_no1_weight+instance.container_no2_weight+instance.container_no3_weight+instance.container_no4_weight+instance.container_no5_weight)) + instance.other_expense + instance.bank_charges
            return (instance.price*instance.weight) + instance.other_expense + instance.broker_charges
        else:
            # return (instance.price*((instance.container_no1_weight+instance.container_no2_weight+instance.container_no3_weight+instance.container_no4_weight+instance.container_no5_weight)/1000)*instance.dollar_rate) + instance.clearing_charges + instance.other_expense + instance.bank_charges
            return (instance.price*((instance.weight))*instance.dollar_rate) 
            + instance.clearing_charges + instance.other_expense + instance.bank_charges 
            + instance.wh_tax + instance.lc_commission + instance.accountant_charges 
            + instance.insurance

    def get_prev_balance(self, instance):
        
        # sales_revenue = Import.objects.filter(Q(creation_time__lt=instance.creation_time)
        #                                       & Q(company=instance.company) & Q(transaction_type='1')).aggregate(total=Sum((F('price') * (F('container_no1_weight')+F('container_no2_weight')+F('container_no3_weight')+F('container_no4_weight')+F('container_no5_weight'))) + F('other_expense') + F('bank_charges')))['total']

        sales_revenue = Import.objects.filter(Q(creation_time__lt=instance.creation_time)
                                              & Q(company=instance.company) & Q(transaction_type='1')).aggregate(total=Sum((F('price') * (F('weight'))) + F('other_expense') + F('broker_charges')))['total']

        purchase_revenue = Import.objects.filter(Q(creation_time__lt=instance.creation_time)
                                                 & Q(company=instance.company) & Q(transaction_type='2')).aggregate(total=Sum((F('price') * (F('weight')) * F('dollar_rate')) 
                                                 + F('clearing_charges') + F('other_expense')+ F('bank_charges') + F('wh_tax') 
                                                 + F('lc_commission') + F('accountant_charges') + F('insurance') ))['total']

        received_revenue = Payments.objects.filter(Q(creation_time__lt=instance.creation_time)& Q(company=instance.company) & Q(payment_type='1')).aggregate(total=Sum('amount'))['total']

        sent_revenue = Payments.objects.filter(Q(creation_time__lt=instance.creation_time)& Q(company=instance.company) & Q(payment_type='2')).aggregate(total=Sum('amount'))['total']

        if(not received_revenue):
            received_revenue = 0
        if(not sent_revenue):
            sent_revenue = 0
        if(not sales_revenue):
            sales_revenue = 0
        if(not purchase_revenue):
            purchase_revenue = 0

        return  (sent_revenue + sales_revenue) - (received_revenue + purchase_revenue)

        # if(sales_revenue and purchase_revenue):
        #     return sales_revenue - purchase_revenue
        # elif(purchase_revenue):
        #     return -1*purchase_revenue
        # else:
        #     return sales_revenue

    def get_invoice(self, instance):
        
        if(instance.transaction_type == '1'):
            return instance.price * instance.weight
        else:
            # return instance.price * (instance.weight/1000)
            return instance.price * instance.weight
    
    def get_balance(self, instance):

        amount = self.get_amount(instance)
        prev_balance = self.get_prev_balance(instance)

        if(not amount):
            amount = 0
        if(not prev_balance):
            prev_balance = 0

        if(instance.transaction_type == '1'):
            return prev_balance + amount
        else:
            return prev_balance - amount

    def get_container1_weight_difference(self, instance):

        if instance.transaction_type == '1':
            try:
                container_weight = Import.objects.get(
                    Q(transaction_type='2') & Q(container_no1=instance.container_no1)).container_no1_weight
                if((instance.container_no1_weight > 0) and (container_weight > 0)):
                    return instance.container_no1_weight - container_weight
            except Import.DoesNotExist:
                return 0
        return 0
    
    def get_container2_weight_difference(self, instance):

        if instance.transaction_type == '1':
            try:
                container_weight = Import.objects.get(
                    Q(transaction_type='2') & Q(container_no2=instance.container_no2)).container_no2_weight
                if((instance.container_no2_weight > 0) and (container_weight > 0)):
                    return instance.container_no2_weight - container_weight
            except Import.DoesNotExist:
                return 0
        return 0
    
    def get_container3_weight_difference(self, instance):

        if instance.transaction_type == '1':
            try:
                container_weight = Import.objects.get(
                    Q(transaction_type='2') & Q(container_no3=instance.container_no3)).container_no3_weight
                if((instance.container_no3_weight > 0) and (container_weight > 0)):
                    return instance.container_no3_weight - container_weight
            except Import.DoesNotExist:
                return 0
        return 0
    
    def get_container4_weight_difference(self, instance):

        if instance.transaction_type == '1':
            try:
                container_weight = Import.objects.get(
                    Q(transaction_type='2') & Q(container_no4=instance.container_no4)).container_no4_weight
                if((instance.container_no4_weight > 0) and (container_weight > 0)):
                    return instance.container_no4_weight - container_weight
            except Import.DoesNotExist:
                return 0
        return 0
    
    def get_container5_weight_difference(self, instance):

        if instance.transaction_type == '1':
            try:
                container_weight = Import.objects.get(
                    Q(transaction_type='2') & Q(container_no5=instance.container_no5)).container_no5_weight
                if((instance.container_no5_weight > 0) and (container_weight > 0)):
                    return instance.container_no5_weight - container_weight
            except Import.DoesNotExist:
                return 0
        return 0
    


class PaymentsReadSerializer(serializers.ModelSerializer):
    prev_payment = serializers.SerializerMethodField()
    balance = serializers.SerializerMethodField()
    company = CompanySerializer(read_only=True)
    bank = BankSerializer(read_only=True)
    payment_type = serializers.SerializerMethodField()
    payment_choice_type = serializers.SerializerMethodField()
    

    class Meta:
        model = Payments
        fields = ('id', 'company', 'bank', 'date', 'container_no', 'amount',
                  'payment_type', 'payment_choice_type', 'prev_payment', 'balance')

    

    def get_payment_type(self, instance):
        return instance.get_payment_type_display()
    
    def get_payment_choice_type(self, instance):
        return instance.get_payment_choice_type_display()

    def get_prev_payment(self, instance):
        # import pdb
        # pdb.set_trace()
        received_revenue = 0
        sent_revenue = 0
        sales_revenue =0 
        purchase_revenue = 0
        received_revenue = Payments.objects.filter(Q(creation_time__lt=instance.creation_time)& Q(company=instance.company) & Q(payment_type='1')).aggregate(total=Sum('amount'))['total']

        sent_revenue = Payments.objects.filter(Q(creation_time__lt=instance.creation_time)& Q(company=instance.company) & Q(payment_type='2')).aggregate(total=Sum('amount'))['total']
        
        sales_revenue = Import.objects.filter(Q(creation_time__lt=instance.creation_time)
                                              & Q(company=instance.company) & Q(transaction_type='1')).aggregate(total=Sum((F('price') * (F('weight'))) 
                                              + F('other_expense') + F('broker_charges')))['total']

        purchase_revenue = Import.objects.filter(Q(creation_time__lt=instance.creation_time)
                                                 & Q(company=instance.company) & Q(transaction_type='2')).aggregate(total=Sum((F('price') * (F('weight')/1000) * F('dollar_rate')) 
                                                 + F('clearing_charges') + F('other_expense')+ F('bank_charges') + F('wh_tax') 
                                                 + F('lc_commission') + F('accountant_charges') + F('insurance') ))['total']

        if(not received_revenue):
            received_revenue = 0
        if(not sent_revenue):
            sent_revenue = 0
        if(not sales_revenue):
            sales_revenue = 0
        if(not purchase_revenue):
            purchase_revenue = 0
        
        # if(received_revenue and sent_revenue):
        #     return (sent_revenue + sales_revenue) - (received_revenue + purchase_revenue)
        # elif(received_revenue):
        #     return -1*(received_revenue + purchase_revenue)
        # elif(sent_revenue):
        #     return (sent_revenue + sales_revenue)
        # else:
        return  (sent_revenue + sales_revenue) - (received_revenue + purchase_revenue)

    def get_balance(self, instance):

        amount = instance.amount
        prev_balance = self.get_prev_payment(instance)

        if(not amount):
            amount = 0
        if(not prev_balance):
            prev_balance = 0

        if(instance.payment_type == '1'):
            return prev_balance - amount
        else:
            return prev_balance + amount


class PaymentsCreateUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payments
        fields = '__all__'
