{
      "name": "Remote Django App",
      "type": "python",
      "request": "attach",
      "pathMappings": [
        {
          "localRoot": "${workspaceFolder}",
          "remoteRoot": "/code"
        }
      ],
      "port": 8888,
      "host": "localhost"
    },