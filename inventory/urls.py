from django.urls import include, path
from rest_framework import routers
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
# from jet_django.urls import jet_urls
from trcs import views

# from rest_framework_simplejwt.views import (
#     TokenObtainPairView,
#     TokenRefreshView,
# )

urlpatterns = [

    # path('jet/', include('jet.urls', 'jet')),
    # path('grappelli/', include('grappelli.urls')),
    path('admin/', admin.site.urls),
    path('api/company/', views.CompanyList.as_view()),
    path('api/company/<int:pk>/', views.CompanyDetail.as_view()),
    ## Commodity URLS
    path('api/commodity/create', views.CommodityCreate.as_view()),
    path('api/commodity/update/<int:pk>/', views.CommodityUpdate.as_view()),
    path('api/commodity/', views.CommodityList.as_view()),
    path('api/commodity/<int:pk>/', views.CommodityDetail.as_view()),

    ## Bank URLS
    path('api/bank/', views.BankList.as_view()),
    path('api/bank/<int:pk>/', views.BankDetail.as_view()),

    ## Imports URLS
    path('api/import/create', views.ImportCreate.as_view()),
    path('api/import/update/<int:pk>/', views.ImportUpdate.as_view()),
    path('api/import/', views.ImportList.as_view()),
    path('api/import/<int:pk>/', views.ImportDetail.as_view()),
    ## Payments URLS
    path('api/payment/create', views.PaymentsCreate.as_view()),
    path('api/payment/update/<int:pk>/', views.PaymentsUpdate.as_view()),
    path('api/payment/', views.PaymentsList.as_view()),
    path('api/payment/<int:pk>/', views.PaymentsDetail.as_view()),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

]
urlpatterns = format_suffix_patterns(urlpatterns)